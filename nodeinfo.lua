local curl = require("cURL.safe")
local json = require("cjson.safe")
local lustache = require("lustache")
local web_sanitize = require("web_sanitize")

local function html_list(entries, rewrite)
	local result = {}
	for i = 1, #entries do
		if type(entries[i]) == "string" then
			table.insert(result, "<li>" .. rewrite(entries[i]) .. "</li>")
		end
	end
	return table.concat(result, "\n")
end

local function features(software, metadata)
	if metadata.features then
	    return html_list(metadata.features, function(v)
			if v == "pleroma_api" then return "Pleroma API"
			elseif v == "mastodon_api" then return "Mastodon API"
			elseif v == "mastodon_api_streaming" then return "Mastodon Streaming API"
			elseif v == "gopher" then return "Gopher"
			elseif v == "media_proxy" then return "Media proxy"
			elseif v == "chat" then return "Chat"
			elseif v == "suggestions" then return "Suggestions"
			elseif v == "relay" then return "Relay"
			elseif v == "safe_dm_mentions" then return "Safe DM Mentions"
			end
			return web_sanitize.extract_text(v)
		end)
	elseif software == "misskey" then
		-- some misskey specifics
		local features = {}
		if metadata.maxNoteTextLength then
			table.insert(features,
				string.format("Character limit: %d", metadata.maxNoteTextLength))
		end
		if metadata.enableEmail then
			table.insert(features, "Email")
		end
		if metadata.enableTwitterIntegration then
			table.insert(features, "Twitter integration")
		end
		if metadata.enableGithubIntegration then
			table.insert(features, "GitHub integration")
		end
		if metadata.enableDiscordIntegration then
			table.insert(features, "Discord integration")
		end
		if metadata.enableServiceWorker then
			table.insert(features, "Service worker")
		end
		return html_list(features, function(v)
			return v
		end)
	end
end

local function staff(metadata)
	if metadata.staffAccounts then
	    return html_list(metadata.staffAccounts, function(v)
			local full = web_sanitize.extract_text(v)
			local short = string.match(full, "[A-Za-z0-9_-]*$")
			return string.format('<a href="%s">%s</a>', full, short)
		end)
	elseif metadata.maintainer then
		if metadata.maintainer.name then
			return string.format('<li><a href="mailto:%s">%s</a></li>',
				web_sanitize.extract_text(metadata.maintainer.email),
				web_sanitize.extract_text(metadata.maintainer.name))
		end
	end
end

local function append_mrf_simple_type(t, mrf, filter_name)
	for i = 1, #mrf do
		table.insert(t,
			string.format('<tr><td class="blocked-host">%s</td><td>%s</td></tr>',
			web_sanitize.extract_text(mrf[i]), filter_name))
	end
end

local function mrf_simple(mrf)
	local result = {}
	append_mrf_simple_type(result, mrf.accept, "accept")
	append_mrf_simple_type(result, mrf.media_nsfw, "mark media NSFW")
	append_mrf_simple_type(result, mrf.media_removal, "remove media")
	append_mrf_simple_type(result,
		mrf.federated_timeline_removal, "remove from federated timeline")
	append_mrf_simple_type(result, mrf.reject, "reject all messages")
	return table.concat(result, "\n")
end

local function description(metadata)
	local text = ""
	if metadata.nodeDescription then
		text = text .. web_sanitize.sanitize_html(metadata.nodeDescription)
	elseif metadata.description then
		text = text .. web_sanitize.sanitize_html(metadata.description)
	end
	text = string.gsub(text, "\n", "<br>")
	if metadata.announcements then
		for i = 1, #metadata.announcements do
			text = text .. string.format('<h3>%s</h3>',
				web_sanitize.sanitize_html(metadata.announcements[i].title))
			text = text .. string.format('<p>%s</p>',
				web_sanitize.sanitize_html(metadata.announcements[i].text))
		end
	end
	return text
end

local function data_unit(amount)
	if amount >= 1000000000 then
		return string.format("%dGB", amount / 1000000000)
	elseif amount >= 1000000 then
		return string.format("%dMB", amount / 1000000)
	elseif amount >= 1000 then
		return string.format("%dkB", amount / 1000)
	else
		return string.format("%d bytes", amount)
	end
end

local function fetch_json(url)
	local queue = {}
	local request = curl.easy()
		:setopt_customrequest("GET")
		:setopt_url(url)
		:setopt_httpheader({ "Accept: application/json" })
		:setopt_writefunction(function(buffer)
			table.insert(queue, buffer)
		end)
	if not request:perform() or request:getinfo(curl.INFO_RESPONSE_CODE) ~= 200 then
		return nil, true
	end
	request:close()
	return json.decode(table.concat(queue, ""))
end

local function fail(message)
	print("Status: 500 Internal Server Error")
	print("Content-Type: text/plain")
	print("")
	print(message)
	os.exit(1)
end

local instance = "pleroma.soykaf.com"

local query = os.getenv("QUERY_STRING")
if query and #query > 1 then
	instance = query
end

local json, error = fetch_json("https://" .. instance .. "/.well-known/nodeinfo")
if error or type(json) ~= "table" then
	fail(string.format("Couldn't fetch nodeinfo metadata %s. Either it doesn't support nodeinfo, or it's currently offline.", instance))
end

local nodeinfo_url = nil

for i = 1, #json.links do
	if string.match(json.links[i].rel, "http://nodeinfo.diaspora.software/ns/schema/2%.%d*") then
		nodeinfo_url = json.links[i].href
		break
	end
end

if nodeinfo_url == nil then
	fail(string.format("%s doesn't support nodeinfo 2.0.", instance))
end

json, error = fetch_json(nodeinfo_url)
if error or type(json) ~= "table" then
	fail(string.format("Error fetching/decoding nodeinfo JSON from %s.", instance))
end

local template = [[
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{nodeName}} | Fediverse Node</title>
<link rel="stylesheet" href="nodeinfo.css">
</head>
<body>
<header>
<h1>About <a href="{{nodeUrl}}">{{nodeName}}</a></h1>
{{{nodeDescription}}}
</header>
<section>
<h2>Usage</h2>
<ul>
<li>Number of reported local posts: <strong>{{localPosts}}</strong></li>
<li>Number of reported registered accounts: <strong>{{localUsers}}</strong></li>
<li>Open registrations? <strong>{{openRegistrations}}</strong></li>
<li>Private? <strong>{{private}}</strong></li>
</ul>
</section>
{{#staff}}
<section>
<h2>Staff</h2>
<ul>
{{{staff}}}
</ul>
</section>
{{/staff}}
<section class="software">
<h2>Software</h2>
<ul>
<li>Name: <strong>{{softwareName}}</strong></li>
<li class="software-version">Version: <strong>{{softwareVersion}}</strong></li>
</ul>
<div class="software-properties">
<section>
<h3>Protocols</h3>
<ul>
{{{protocols}}}
</ul>
</section>
{{#features}}
<section>
<h3>Features</h3>
<ul>
{{{features}}}
</ul>
</section>
{{/features}}
{{#postFormats}}
<section>
<h3>Post formats</h3>
<ul>
{{{postFormats}}}
</ul>
</section>
{{/postFormats}}
{{#uploadLimits}}
<section>
<h3>Upload limits</h3>
<ul>
<li>General: <strong>{{uploadLimitGeneral}}</strong></li>
<li>Avatars: <strong>{{uploadLimitAvatar}}</strong></li>
<li>Backgrounds: <strong>{{uploadLimitBackground}}</strong></li>
<li>Banners: <strong>{{uploadLimitBanner}}</strong></li>
</ul>
</section>
{{/uploadLimits}}
</div>
</section>
{{#has_federation_policies}}
<section>
<h2>Federation Policies</h2>
{{#mrf_policies}}
<section>
<h3>MRF Policies</h3>
<ul>
{{{mrf_policies}}}
</ul>
</section>
{{/mrf_policies}}
{{#mrf_simple}}
<section>
<h3>MRF Simple</h3>
<table>
<thead>
<tr>
<th>Instance</th>
<th>Fate</th>
</tr>
</thead>
<tbody>
{{{mrf_simple}}}
</tbody>
</table>
</section>
{{/mrf_simple}}
{{#quarantined}}
<section>
<h3>Quarantined instances</h3>
<p>Quarantined instances will not be sent private posts.</p>
<ul>
{{{quarantined}}}
</ul>
</section>
{{/quarantined}}
</section>
{{/has_federation_policies}}
<footer>
<p><a href="https://gitlab.com/niaa/nodeinfo.lua">nodeinfo.lua</a> is a
<a href="https://catgirl.science/nia">Catgirl Science Labs</a> project.</p>
</footer>
</body>
</html>
]]

print("Content-Type: text/html")
print("")
print(lustache:render(template, {
	nodeUrl = "https://" .. instance,
	nodeName = (json.metadata and json.metadata.nodeName) or instance,
	nodeDescription = description(json.metadata),
	softwareName = json.software.name,
	softwareVersion = json.software.version,
	localPosts = (json.usage.localPosts and string.format("%d", json.usage.localPosts)) or
		(json.usage.localComments and string.format("%d", json.usage.localComments)) or "unknown",
	localUsers = json.usage.users.total and string.format("%d", json.usage.users.total) or "unknown",
	openRegistrations = json.openRegistrations and "Yes" or "No",
	private = (json.metadata and json.metadata.private) and "Yes" or "No",
	staff = json.metadata and staff(json.metadata),
	features = json.metadata and features(json.software.name, json.metadata),
	protocols = html_list(json.protocols, function(v)
		if v == "ostatus" then return "OStatus"
		elseif v == "activitypub" then return "ActivityPub" end
		return web_sanitize.extract_text(v)
	end),
	uploadLimits = json.metadata and json.metadata.uploadLimits,
	uploadLimitGeneral = json.metadata and json.metadata.uploadLimits and
	    data_unit(json.metadata.uploadLimits.general),
	uploadLimitAvatar = json.metadata and json.metadata.uploadLimits and
	    data_unit(json.metadata.uploadLimits.avatar),
	uploadLimitBackground = json.metadata and json.metadata.uploadLimits and
	    data_unit(json.metadata.uploadLimits.background),
	uploadLimitBanner = json.metadata and json.metadata.uploadLimits and
	    data_unit(json.metadata.uploadLimits.banner),
	postFormats = json.metadata and json.metadata.postFormats and
	    html_list(json.metadata.postFormats,
		function(v)
			if v == "text/plain" then return "Plain text"
			elseif v == "text/html" then return "HTML"
			elseif v == "text/markdown" then return "Markdown" end
			return web_sanitize.extract_text(v)
		end),
	has_federation_policies = not (not json.metadata.federation),
	mrf_policies = json.metadata and json.metadata.federation and
	    json.metadata.federation.mrf_policies and
	    html_list(json.metadata.federation.mrf_policies,
		function(v) return "<samp>" .. web_sanitize.extract_text(v) .. "</samp>" end),
	mrf_simple = json.metadata and json.metadata.federation and
	    json.metadata.federation.mrf_simple and
	    mrf_simple(json.metadata.federation.mrf_simple),
	quarantined = json.metadata and json.metadata.federation and
	    json.metadata.federation.quarantined_instances and
	    #json.metadata.federation.quarantined_instances > 1 and
	    html_list(json.metadata.federation.quarantined_instances,
		function(v) return web_sanitize.extract_text(v) end),
}))
